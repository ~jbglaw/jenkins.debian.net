About the builtin-pho/buildinfos.d.n setup on ionos7-amd64
==========================================================
:Author:           Holger Levsen
:Authorinitials:   holger
:EMail:            holger@layer-acht.org
:Status:           working, in progress
:lang:             en
:Doctype:          article
:License:          GPLv2

== Scope

Describe how the builtin-pho setup and buildinfos.debian.net
on ionos7-amd64 are done.

builtin-pho stores information about .buildinfo files from
ftp-master.debian.org in a postgresql database.

buildinfos.debian.net is an attempt to make the files from
/srv/ftp-master.debian.org/buildinfo/ on coccia.debian.org
accessable to the world.

=== builtin=pho initial setup

as user buildinfo on ionos7:

$ cd
$ git clone https://salsa.debian.org/bremner/builtin-pho.git

and then follow the steps in the README

=== builtin-pho re-setup

A re-setup is eg needed whenever the suites change.

as user buildinfo:

$ cd ~/builtin-pho
$ bash drop-all-tables.sh 
$ bash make-types.sh
$ bash update-packages.sh		# takes a few minutes
$ python3 index-buildinfo.py -v		# takes hours, run in screen!

as user postgres:

$ psql buildinfo
psql (11.7 (Debian 11.7-0+deb10u1))
Type "help" for help.

buildinfo=# grant connect on database buildinfo to jenkins;
GRANT
buildinfo=# grant usage on schema public to jenkins;
GRANT
buildinfo=# grant select on all tables in schema public to jenkins;
GRANT
buildinfo=#

=== cronjobs and jenkins jobs for builtin-pho and buildinfos.d.n

cronjobs:

 * in this git repo: see hosts/ionos7-amd64/etc/cron.d/builtin-pho
   - runs on ionos7 every hour
   - updates packages files and db
 * in this git repo: see bin/rsync2buildinfos.debian.net
   - runs on coccia.debian.org every other hour via holger's crontab
   - rsyncs buildinfo files from coccia to ionos7 AKA buildinfos.debian.net

jenkins jobs:

 * reproducible_html_builtin-pho:
   - runs on ionos7 every hour
   - creates html pages for tests.r-b.o/debian
 * reproducible_html_rsync_builtin-pho
   - runs on jenkins every hour
   - copies html pages for tests.r-b.o/debian from ionos7 to jenkins
 * reproducible_pool_buildinfos:
   - runs on ionos7 every other hour
   - links files for buildinfos.debian.net from date to pool structure


// vim: set filetype=asciidoc:
