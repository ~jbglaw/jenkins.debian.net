#!/bin/bash
# vim: set noexpandtab:

# Copyright 2014-2021 Holger Levsen <holger@layer-acht.org>
#         © 2018-2023 Mattia Rizzolo <mattia@debian.org>
# released under the GPLv2

DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh

# support different suites
if [ -z "$1" ] ; then
	SUITE="unstable"
else
	SUITE="$1"
fi

#
# create script to configure a pbuilder chroot
#
create_customized_tmpfile() {
	TMPFILE=$1
	shift
	cat >> $TMPFILE <<- EOF
#
# this script is run within the pbuilder environment to further customize initially
#
echo
echo "Preseeding man-db/auto-update to false"
echo "man-db man-db/auto-update boolean false" | debconf-set-selections
echo
echo "Configuring dpkg to not fsync()"
echo "force-unsafe-io" > /etc/dpkg/dpkg.cfg.d/02speedup
echo
EOF
	. /srv/jenkins/bin/jenkins_node_definitions.sh
	get_node_information "$HOSTNAME"
	if "$NODE_RUN_IN_THE_FUTURE" ; then
		cat >> $TMPFILE <<- EOF
			echo "Configuring APT to ignore the Release file expiration"
			sed -i 's,^deb ,deb [check-valid-until=no] ,g' /etc/apt/sources.list
			echo
		EOF
	fi

}

create_setup_our_repo_tmpfile() {
	TMPFILE=$1
	shift
	cat >> $TMPFILE <<- EOF
#
# this script is run within the pbuilder environment to further customize once more
#
echo "Configure the chroot to use the reproducible team experimental archive..."
echo "-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBFQsy/gBEADKGF55qQpXxpTn7E0Vvqho82/HFB/yT9N2wD8TkrejhJ1I6hfJ
zFXD9fSi8WnNpLc6IjcaepuvvO4cpIQ8620lIuONQZU84sof8nAO0LDoMp/QdN3j
VViXRXQtoUmTAzlOBNpyb8UctAoSzPVgO3jU1Ngr1LWi36hQPvQWSYPNmbsDkGVE
unB0p8DCN88Yq4z2lDdlHgFIy0IDNixuRp/vBouuvKnpe9zyOkijV83Een0XSUsZ
jmoksFzLzjChlS5fAL3FjtLO5XJGng46dibySWwYx2ragsrNUUSkqTTmU7bOVu9a
zlnQNGR09kJRM77UoET5iSXXroK7xQ26UJkhorW2lXE5nQ97QqX7igWp2u0G74RB
e6y3JqH9W8nV+BHuaCVmW0/j+V/l7T3XGAcbjZw1A4w5kj8YGzv3BpztXxqyHQsy
piewXLTBn8dvgDqd1DLXI5gGxC3KGGZbC7v0rQlu2N6OWg2QRbcVKqlE5HeZxmGV
vwGQs/vcChc3BuxJegw/bnP+y0Ys5tsVLw+kkxM5wbpqhWw+hgOlGHKpJLNpmBxn
T+o84iUWTzpvHgHiw6ShJK50AxSbNzDWdbo7p6e0EPHG4Gj41bwO4zVzmQrFz//D
txVBvoATTZYMLF5owdCO+rO6s/xuC3s04pk7GpmDmi/G51oiz7hIhxJyhQARAQAB
tC5EZWJpYW4gUmVwcm9kdWNpYmxlIEJ1aWxkcyBBcmNoaXZlIFNpZ25pbmcgS2V5
iQJUBBMBCAA+AhsDBQsJCAcDBRUKCQgLBRYDAgEAAh4BAheAFiEESbZXRzbQtjfM
NwHqXbfKZ+pZox8FAmOLQqUFCSIqea0ACgkQXbfKZ+pZox/nVw/+IlxfezxH56Qs
sh+vEbNLH6RHc7RlBc0i6uDxqYEfJe8ofIxW9TfUPPljVsGupwUcj1fJSTyBm4rZ
xLlAz/icmnATTFHFqQY7y7byQcK86qtLd0SpKuf088cmIB1F5h1Ut9fHtJUmCxiG
EAHWexFEixzvaRROm7pZTeLjZv07KrAkCDVNO2CiNYA9Ja6pZBgmk9mwSduZQBuX
fG5iUp0JLVAqsSQHz6+9NP5spXHg/Za3bUTdXE/YyI6V9DvwGXNu/HKQfyPpxDcH
VLRGcgDI4lQi0gsWpJP+2ZmLin0eMNCrdnMs9OLwASE9YPiIMGRsH4biC8ku+u6H
ZfQHSR8XiDAqpvm2xTSmROcwuhiGZYm1mIN3/J2AAzuJtGs3V1jHst0sI8YD9AqQ
UsePpgkQsnjc7+q3IdWjWBEGm4kGThq/i0zevLmDc5TiXCaDbS34Vgl37KsBcqQb
fRNd8grAnGTF1u2ssuN+CaMrsAWOcWeVU7DuDBboz2GdK4VD9KnzXgC/Ee9pB3fb
if6W02T+cxsDSW1dxvTEca5+yi2IOqe6lO7sR7X8KNuyBbTXLgdITpLRtNvaNjDa
Li9iZuBQQQsvyCT3BCOmsTg7AINlV4Xpy1tD/QvSiV9+TSZvCbtd+Q45NGDolurX
H8IP+0wasJJj0Q7j7rDL3i9vABF7t5qJAhwEEAEIAAYFAlQxhfAACgkQEDeO/CCA
CAzd7xAAogFZRfcV+v43EFgGDUU2YjzSsv7I5hCSpqd0PmZV49nGNbItk8tr3IUb
ioe+1MAe71kkObi2sTJk7Ar1lHaAQH3Dod7A7jFaaGgT1e9alinCopWewHg7+Yo3
JIrjE1qWq70Yu1CTXfe4r+qlVq8bLhtnO3DQwxYKXBZ1JZMlwNPi9Icmr9V7KDGX
XNo2crLtRfpQ/HIsWOIo8A4wb302NoHKXr817gOBxLTp15W5YqDu0eYxyAYkz3vc
2ZZb+/6NUtKw9pwfbhwPJ7vGUNptLO/zLsFqBni40xmQUlbfrobYEc+msdvjWvie
iy+ACb8vtuac0Kt/Z4Qm2UWXtdKxGQ6oQp9NIMz2CtnasyNlpRdu+6C61rMjHY8X
GZhYTjCDfjPL/CUzAtd/5UOPnf0zaIxpYQ7xDZ/mwsQfCQGpgUhC3qWjMs3bOxaQ
UvNmqW4k1IyggTm84fchYI5+zMf1mfGEWtPD3R3AOgbI37Jz/K//OX+mSoH2RtmL
PPjLH+SeKtDNY02LJY29VBq1Ho703uUQJJTJbuiaFbu70ZzLXO0s4dxnx4pK2I8B
daq2l7hl4K/Hao4y7Oc+COsAznBb0YshxBJrPx/cpIwDukpd+1Q8Klt06wsG/xtF
CSjV5hj8yHBm7Qkb4rNDiLxBNUgUIxI8EurIsFlosv/2/39xWqWJAhwEEAECAAYF
AlQxFdAACgkQg4LJXCkCPfmQKhAAqyE9w4/sKyGPuc6vHQM96qCUCtjeAOYoK4YL
9kydrEXjcyGE3XfNUSst5WBV5+oiCQhwJ68zwtSQGlsgqNYCSinujgd3jyYg/cvh
0IKQ02OP5uMTmw2N5s4TpbR36CyO7zvg+ZdtN9Y42OGiE4QxAPqWosXp8yPEivWe
7RWhHEumeG7ik5+Vgw/Mp3E/ZOSwE6bQmK2TKgI4x+zTbtSJx2uxKuVO1SWBAtTe
/4fyZ/HkLmezDuPIxnnfaUwyBvSc61NxGhhJiYovCl4Bk7E1WJt4KfcdQxJkYNkI
vlIUpwZJauOFCXwP1ynatue8GURSQCQSS1Wm6sQoQtr+cqk3Y+jVFERo0FXP1YCd
e7GK4d/Bhz6TKO0E51t6Vlm41D8KdF6Z6xrnW0Ez9gtpP5/7z291W18yoqoPpMUf
vF/EpyysXlrhfixCr9nw8iJUNo7E89UMhH0SbphFkF5yc1WZ5PvhTu3Rp2hxJZrH
kaYfEczUHEDj5h8HVuqeXvDE28OJjSQ+1HpsYmQ39COdvWc7GA4zef4nw1RlBKWc
P4KHwGSPD0mo6n6tMYRyQd2rgv2STlcLyjFUmXJo63lMvP+MgY8x/NfbvnCFuLFU
zN9SkLp+sfs1gJ4/O3c9laUrlQ/37lYYkd7WrIxBTx6HmRWK3jSb4G96HROHh4ay
aDdBspI=
=mIN3
-----END PGP PUBLIC KEY BLOCK-----" > /etc/apt/trusted.gpg.d/reproducible.asc
echo 'deb http://tests.reproducible-builds.org/debian/repository/debian/ ./' > /etc/apt/sources.list.d/reproducible.list
echo "Package: *
Pin: release o=reproducible
Pin-Priority: 1001" > /etc/apt/preferences.d/reproducible
echo
apt-get update
apt-get -y upgrade
apt-get install -y $@
echo
apt-cache policy
echo
dpkg -l
echo
for i in \$(dpkg -l |grep ^ii |awk -F' ' '{print \$2}'); do   apt-cache madison "\$i" | head -1 | grep reproducible-builds.org || true  ; done
echo
EOF
}


#
# setup pbuilder for reproducible builds
#
setup_pbuilder() {
	SUITE=$1
	shift
	NAME=$1
	shift
	PACKAGES="$@"						# from our repo
	EXTRA_PACKAGES="locales-all fakeroot disorderfs"	# from sid
	echo "$(date -u) - creating /var/cache/pbuilder/${NAME}.tgz now..."
	TMPFILE=$(mktemp --tmpdir=$TEMPDIR pbuilder-XXXXXXXXX)
	LOG=$(mktemp --tmpdir=$TEMPDIR pbuilder-XXXXXXXX)
	if [ "$SUITE" = "experimental" ] ; then
		SUITE=unstable
		echo "echo 'deb $MIRROR experimental main' > /etc/apt/sources.list.d/experimental.list" > ${TMPFILE}
		echo "echo 'deb-src $MIRROR experimental main' >> /etc/apt/sources.list.d/experimental.list" >> ${TMPFILE}
	fi
	# use host apt proxy configuration for pbuilder too
	if [ -n "$http_proxy" ] ; then
		echo "echo '$(cat /etc/apt/apt.conf.d/80proxy)' > /etc/apt/apt.conf.d/80proxy" >> ${TMPFILE}
		pbuilder_http_proxy="--http-proxy $http_proxy"
	fi
	# setup base.tgz
	sudo pbuilder --create $pbuilder_http_proxy --basetgz /var/cache/pbuilder/${NAME}-new.tgz --distribution $SUITE --debootstrapopts --no-merged-usr --extrapackages "$EXTRA_PACKAGES" --loglevel D

	# customize pbuilder
	create_customized_tmpfile ${TMPFILE}
	if [ "$DEBUG" = "true" ] ; then
		cat "$TMPFILE"
	fi
	sudo pbuilder --execute $pbuilder_http_proxy --save-after-exec --basetgz /var/cache/pbuilder/${NAME}-new.tgz -- ${TMPFILE} | tee ${LOG}
	rm ${TMPFILE}

	# add repo only for experimental and unstable - keep != unstable "real" (and sid progressive!)
	if [ "$SUITE" = "unstable" ] || [ "$SUITE" = "experimental" ]; then
		# apply further customisations, eg. install $PACKAGES from our repo
		create_setup_our_repo_tmpfile ${TMPFILE} "${PACKAGES}"
		if [ "$DEBUG" = "true" ] ; then
			cat "$TMPFILE"
		fi
		sudo pbuilder --execute $pbuilder_http_proxy --save-after-exec --basetgz /var/cache/pbuilder/${NAME}-new.tgz -- ${TMPFILE} | tee ${LOG}
		rm ${TMPFILE}
		if [ -n "$PACKAGES" ] ; then
			# finally, confirm things are as they should be
			echo
			echo "Now let's see whether the correct packages where installed..."
			for PKG in ${PACKAGES} ; do
				grep -E "http://tests.reproducible-builds.org/debian/repository/debian(/|) ./ Packages" ${LOG} \
					| grep -v grep | grep "${PKG} " \
					|| ( echo ; echo "Package ${PKG} is not installed at all or probably rather not in our version, so removing the chroot and exiting now." ; sudo rm -v /var/cache/pbuilder/${NAME}-new.tgz ; rm $LOG ; exit 1 )
			done
		fi
	fi

	sudo mv /var/cache/pbuilder/${NAME}-new.tgz /var/cache/pbuilder/${NAME}.tgz
	# create stamp file to record initial creation date minus some hours so the file will be older than 24h when checked in <24h...
	touch -d "$(date -u -d '6 hours ago' '+%Y-%m-%d %H:%M')" /var/log/jenkins/${NAME}.tgz.stamp
	rm ${LOG}
}

#
# main
#
BASETGZ=/var/cache/pbuilder/$SUITE-reproducible-base.tgz
STAMP=/var/log/jenkins/$SUITE-reproducible-base.tgz.stamp

if [ -f "$STAMP" ] ; then
	if [ -f "$STAMP" -a $(stat -c %Y "$STAMP") -gt $(date +%s) ]; then
		if [ $(stat -c %Y "$STAMP") -gt $(date +%s -d "+ 6 months") ]; then
			echo "Warning: stamp file is too far in the future, assuming something is wrong and deleting it"
			rm -v "$STAMP"
		else
			echo "stamp file has a timestamp from the future."
			exit 1
		fi
	fi
fi

OLDSTAMP=$(find $STAMP -mtime +1 -exec ls -lad {} \; || echo "nostamp")
if [ -n "$OLDSTAMP" ] || [ ! -f $BASETGZ ] || [ ! -f $STAMP ] ; then
	if [ ! -f $BASETGZ ] ; then
		echo "No $BASETGZ exists, creating a new one..."
	else
		echo "$BASETGZ outdated, creating a new one..."
	fi
	setup_pbuilder $SUITE $SUITE-reproducible-base # list packages which must be installed from our repo here
else
	echo "$BASETGZ not old enough, doing nothing..."
fi
echo
