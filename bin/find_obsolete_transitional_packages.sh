#!/bin/bash

# Copyright 2017-2023 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

#
# run with "bug" as first parameter for interactive mode which will fire up mutt for $MAX buggy packages
MAX=20
#
#
if [ -z "$1" ] ; then
	echo "Call $(basename $0) [bug] NEXT STABLE OLDSTABLE"
	echo "         bug to enter manual mode"
	echo "         NEXT suite which is being developed, eg 'trixie'"
	exit 0
fi

DEBUG=false
if [ -f /srv/jenkins/bin/common-functions.sh ] ; then
	. /srv/jenkins/bin/common-functions.sh
	common_init "$@"
else
	#normally defined in common-functions.sh
	export MIRROR=http://deb.debian.org/debian
	#for quicker development:
	PACKAGES[0]=/home/schroots/trixie/var/lib/apt/lists/deb.debian.org_debian_dists_trixie_main_binary-amd64_Packages
	PACKAGES[1]=/var/lib/apt/lists/deb.debian.org_debian_dists_bookworm_main_binary-amd64_Packages
	PACKAGES[2]=/home/schroots/sid/var/lib/apt/lists/deb.debian.org_debian_dists_sid_main_binary-amd64_Packages
fi

#
# above only examples are shown for interactivly running this script.
#
# the jenkins job is solely configured in job-cfg/obsolete-transitional.yaml
#

if ! which chdist ; then
	echo "Please install devscripts."
	exit 1
elif ! which grep-dctrl ; then
	echo "Please install grep-dctrl."
	exit 1
fi

if [ "$1" = "bug" ] ; then
	MANUAL_MODE=true
	echo "Entering manual bug filing mode."
	shift
	echo "For forky, please make sure to also list packages which have those transitional packages as build-depends, depends, recommends, suggests or in enhances. h01ger: see ~/bin/init_schroot where you took some notes..."
	read a
	echo "exiting so you do this :-D"
	exit 1
else
	MANUAL_MODE=false
fi


LANG="en_US.UTF-8"
ARCH=amd64
NEXT="$1"
STABLE="$2"
OLDSTABLE="$3"
SUITES="$OLDSTABLE $STABLE sid"



# transitional packages we know bugs have been filed about…
# since stretch release
BUGGED="multiarch-support jadetex dh-systemd libpcap-dev transfig myspell-it myspell-sl python-gobject ttf-dejavu ttf-dejavu-core ttf-dejavu-extra libav-tools netcat gnupg2 libkf5akonadicore-bin qml-module-org-kde-extensionplugin myspell-ca myspell-en-gb myspell-sv-se myspell-lt khelpcenter4 libqca2-plugin-ossl gambas3-gb-desktop-gnome git-core gperf-ace libalberta2-dev asterisk-prompt-it libatk-adaptor-data kdemultimedia-dev kdemultimedia-kio-plugins autoconf-gl-macros autofs5 autofs5-hesiod autofs5-ldap librime-data-stroke5 librime-data-stroke-simp librime-data-triungkox3p pmake host bibledit bibledit-data baloo libc-icap-mod-clamav otf-symbols-circos migemo condor condor-dbg condor-dev condor-doc cscope-el cweb-latex dconf-tools python-decoratortools deluge-torrent deluge-webui django-filter python-django-filter django-tables django-xmlrpc drbd8-utils libefreet1 libjs-flot conky ttf-kacst ttf-junicode ttf-isabella font-hosny-amiri ttf-hanazono ttf-georgewilliams otf-freefont ttf-freefont ttf-freefarsi ttf-liberation libhdf5-serial-dev graphviz-dev git-bzr libgd-gd2-noxpm-ocaml-dev libgd-gd2-noxpm-ocaml ganeti2 ftgl-dev kcron kttsd jfugue verilog iproute iproute-doc ifenslave-2.6  node-highlight libjs-highlight ssh-krb5 libparted0-dev cgroup-bin liblemonldap-ng-conf-perl kdelirc kbattleship kdewallpapers kde-icons-nuvola kdebase-runtime kdebase-bin kdebase-apps libconfig++8-dev libconfig8-dev libdmtx-utils libgcrypt11-dev libixp libphp-swiftmailer libpqxx3-dev libtasn1-3-bin monajat minisat2 mingw-ocaml m17n-contrib lunch qtpfsgui liblua5.1-bitop0 liblua5.1-bitop-dev libtime-modules-perl libtest-yaml-meta-perl scrollkeeper scrobble-cli libqjson0-dbg python-clientform python-gobject-dbg python-pyatspi2 python-gobject-dev python3-pyatspi2 gaim-extendedprefs ptop nowebm node-finished netsurf mupen64plus mpqc-openmpi mono-dmcs  nagios-plugins nagios-plugins-basic nagios-plugins-common nagios-plugins-standard libraspell-ruby libraspell-ruby1.8 libraspell-ruby1.9.1 rcs-latex ffgtk ruby-color-tools libfilesystem-ruby libfilesystem-ruby1.8 libfilesystem-ruby1.9 god rxvt-unicode-ml bkhive scanbuttond python-scikits-learn slurm-llnl slurm-llnl-slurmdbd python-sphinxcontrib-docbookrestapi python-sphinxcontrib-programoutput strongswan-ike strongswan-ikev1 strongswan-ikev2 sushi-plugins task tclcl-dev telepathy-sofiasip tesseract-ocr-dev trac-privateticketsplugin python-twisted-libravatar vdr-plugin-svdrpext qemulator python-weboob-core xfce4-screenshooter-plugin zeroinstall-injector libzookeeper2"
# since buster release
BUGGED="$BUGGED actionaz agda-mode android-tools-adb android-tools-fastboot backintime-gnome backintime-kde beignet bird-bgp chocolate-doom chocolate-common science-astronomy science-astronomy-dev kdesdk-dolphin-plugins ttf-femkeklaver ttf-goudybookletter geany-plugin-gproject gnome-packagekit-session gpgv2 golang-check.v1-dev golang-codegangsta-cli-dev golang-context-dev golang-github-jfrazelle-go-dev golang-snappy-go-dev golang-github-lsegal-gucumber-dev golang-clockwork-dev golang-pq-dev golang-dns-dev golang-mreiferson-httpclient-dev golang-uuid-dev golang-prometheus-client-dev golang-gogoprotobuf-dev golang-go.net-dev golang-go.tools golang-go.tools-dev golang-logrus-dev golang-objx-dev golang-pretty-dev golang-text-dev golang-toml-dev golang-websocket-dev golang-x-text-dev golang-yaml.v2-dev md5deep xul-ext-https-everywhere iptables-dev iptraf ksnapshot kdoctools-dev kio-dev kwayland-dev bsdcpio bsdtar libkf5sysguard5 libkf5sysguard5-data gstreamer0.10-qapt python-libvoikko lua-luxio0 magit mongodb-dev libmpfi0-dev blast2 notmuch-emacs libpango1.0-0 paredit-el ruby-passenger ruby-passenger-doc python-pelican pep8 php-gettext pike8.0-manual pinentry-qt4 kde-config-touchpad plowshare4 polkit-kde-1 gpaco paco puppet-common puppetmaster puppetmaster-passenger python-pytango python3-pytango letsencrypt python-certbot-apache python-certbot-nginx python-dogpile.core python3-dogpile.core ipython-qtconsole ipython3-qtconsole iep gtk-redshift ruby-bson-ext ruby-archive-tar-minitar aterm aterm-ml libsaga golang-github-ubuntu-core-snappy-dev python-spyderlib python3-spyderlib squid3 luasseq ttf-ancient-fonts vnc4server xvnc4viewer aspell-eu-es hunspell-eu-es"
# since bullseye release
BUGGED="$BUGGED acedb-other-belvu acedb-other-dotter libapache2-mod-md libapache2-mod-proxy-uwsgi apertium-es-ca apertium-es-it apt-transport-https auto-complete-el libbabeltrace-ctf-dev libbabeltrace-ctf1 myspell-bg gtk3-engines-breeze buildbot-slave python3-buildbot python3-buildbot-doc python3-buildbot-worker migemo-el compiz-plugins-default science-electronics xul-ext-debianbuttons docker qemu-efi libmaven-exec-plugin-java fonts-hack-otf fonts-hack-ttf fonts-hack-web fonts-roboto-hinted gambas3-gb-gui-opengl gambas3-gb-gui-qt gambas3-gb-gui-qt-webkit gambas3-gb-gui-trayicon libgarcon-1-0-dev gnupg-agent golang-mode golang-go-semver-dev haskell-mode myspell-lv icewm-lite libintellij-annotations-java libintellij-annotations-java-doc natpmp-utils libratbag-tools hunspell-gl-es hunspell-sv-se myspell-hr myspell-pl liblz4-tool mat libegl1-mesa libgl1-mesa-glx libgles2-mesa libwayland-egl1-mesa myspell-pt-br myspell-pt-pt libncurses5-dev libncursesw5-dev libtinfo-dev tor-arm slapd-smbk5pwd pdftk skytools3-ticker libplexus-classworlds2-java libplexus-container-default1.5-java python3-dicom pyotherside idle3 myspell-ru libservlet3.1-java silversearcher-ag-el snap-confine ubuntu-core-launcher libtiff5-dev xul-ext-treestyletab libuim-data verbiste-el libwagon-java x11proto-core-dev x11proto-dri2-dev x11proto-fonts-dev x11proto-gl-dev x11proto-input-dev x11proto-kb-dev x11proto-present-dev x11proto-randr-dev x11proto-record-dev x11proto-render-dev x11proto-scrnsaver-dev x11proto-video-dev x11proto-xext-dev x11proto-xf86dga-dev x11proto-xf86dri-dev x11proto-xf86vidmode-dev x11proto-xinerama-dev libzeroc-ice-java zeroc-ice-utils-java"
# since bookworm release
BUGGED="$BUGGED afl afl++-clang afl-clang afl-doc apertium-af-nl apertium-ca-it apertium-en-ca apertium-id-ms apertium-pt-ca bind9utils dnsutils bsdmainutils budgie-previews-applet bzr bzr-doc bzr-builddeb bzr-email bzr-fastimport bzr-git bzr-stats bzr-upload bzrtools apcalc apcalc-common apcalc-dev ocaml-mode python-celery-common gnome-mpv chipmunk-dev libcollada-dom2.4-dp-dev cryptsetup-run cupp3 python-sortedm2m-data dropbear-run foma-bin ttf-anonymous-pro fonts-sipa-arundina latex-fonts-sipa-arundina fonts-meera-taml fonts-nanum-coding libfreetype6-dev fwupdate libgdk-pixbuf2.0-0 golang-ginkgo-dev golang-github-soniah-gosnmp-dev golang-procfs-dev libgsasl7-dev gstreamer1.0-pulseaudio libhamlib2-perl libhamlib2-tcl lua-hamlib2 python3-libhamlib2 hamster-applet libhdf5-103 libhdf5-cpp-103 libhdf5-mpich-103 libhdf5-openmpi-103 exe-thumbnailer assword libiptc0 kdesignerplugin libjs-leaflet-markercluster libchipcard-libgwenhywfar60-plugins libdnf1 libmikmod-config samplerate-programs libshout3-dev libu2f-udev linphone-nogtk linphone libsensors4-dev mercurial-crecord libgl1-mesa-dev libgles2-mesa-dev mime-support nagios-plugins-contrib nordugrid-arc-plugins-globus dune offlineimap osc-plugins-dput proftpd-basic python3-libusb1 python3-neovim qt5-flatpak-platformtheme qdbus ruby-gnome2 ruby-gnome2-dev runit-systemd libsane scala-mode-el libsdformat6-dev slic3r-prusa snd-gtk-jack snd-gtk-pulse solaar-gnome3 sextractor python3-tempest-horizon tuareg-mode vmdk-stream-converter wireshark-gtk libxcb-util0-dev xrayutilities python3-yubikey-manager zimwriterfs"
# to be continued after the trixie release

echo "Looking at $SUITES for obsolete transitional packages in $NEXT."
BASEPATH=$(ls -1d /tmp/transitional-????? 2>/dev/null || true)
if [ -z "$BASEPATH" ] ; then
	BASEPATH=$(mktemp -t $TMPDIR -d transitional-XXXXX)

	for SUITE in $SUITES ; do
	        mkdir -p $BASEPATH/$SUITE
	        # the "[arch=$ARCH]" is a workaround until #774685 is fixed
	        chdist --data-dir=$BASEPATH/$SUITE --arch=$ARCH create $SUITE-$ARCH "[arch=$ARCH]" $MIRROR $SUITE main
		# in manual mode we don't care about sources, because we don't create a dd-list which needs it
		if $MANUAL_MODE ; then
			sed -i "s#deb-src#\#deb-src#g" $BASEPATH/$SUITE/$SUITE-$ARCH/etc/apt/sources.list
		fi
	        chdist --data-dir=$BASEPATH/$SUITE --arch=$ARCH apt-get $SUITE-$ARCH update
		echo
	done
fi

NR=0
for SUITE in $SUITES ; do
	PACKAGES[$NR]=$(ls $BASEPATH/$SUITE/$SUITE-$ARCH/var/lib/apt/lists/*_dists_${SUITE}_main_binary-${ARCH}_Packages)
       	echo "PACKAGES[$NR] = ${PACKAGES[$NR]}"
	# only in non-interactive mode we care about sources, because we then create a dd-list which needs it
	if ! $MANUAL_MODE ; then
		SOURCES[$NR]=$(ls $BASEPATH/$SUITE/$SUITE-$ARCH/var/lib/apt/lists/*_dists_${SUITE}_main_source_Sources)
		echo "SOURCES[$NR] = ${SOURCES[$NR]}"
	fi
	echo
	let NR=$NR+1
done


BAD=""
BAD_COUNTER=0
GOOD_COUNTER=0
BUGGED_COUNTER=0 ; for i in $BUGGED ; do let BUGGED_COUNTER=$BUGGED_COUNTER+1 ; done
OPEN_COUTER=0
for PKG in $(grep-dctrl -sPackage -n -FDescription "transitional.*package" --ignore-case --regex ${PACKAGES[1]}) ; do
	if [ "${PKG:0:9}" = "iceweasel" ] || [ "${PKG:0:7}" = "icedove" ] || [ "${PKG:0:6}" = "iceowl" ] || [ "${PKG:0:9}" = "lightning" ]; then
		echo "ignore iceweasel, icedove, iceowl, lightning and friends…: $PKG"
		continue
	fi
	if echo " $BUGGED " | grep -q -E " $PKG " ; then
		echo "ignore $PKG because a bug has already been filed."
		let OPEN_COUNTER=$OPEN_COUNTER+1
		continue
	fi
	OLDSTABLE_HIT=$(grep-dctrl -sPackage -n -FDescription "transitional.*package" --ignore-case --regex ${PACKAGES[0]} |grep -E "^$PKG$" || true)
	if [ -z "$OLDSTABLE_HIT" ] ; then
		echo "$PKG not in $OLDSTABLE, so new transitional package, so fine."
	else
		SID_HIT=$(grep-dctrl -sPackage -n -FDescription "transitional.*package" --ignore-case --regex ${PACKAGES[2]} |grep -E "^$PKG$" || true)
		if [ -z "$SID_HIT" ] ; then
			echo "Transitional package $PKG in $OLDSTABLE and $STABLE, but not in sid, yay!"
			let GOOD_COUNTER=$GOOD_COUNTER+1
		else
			let BAD_COUNTER=$BAD_COUNTER+1
			echo
			echo "Warning: $PKG is a transitional package in $OLDSTABLE, $STABLE and sid."
			if ! $MANUAL_MODE ; then
				echo "Please file a bug by runninig this script in interactive mode."
			fi
			echo "SIGH #$BAD_COUNTER transitional packages with no bugs filed found so far."
			BAD="$BAD $PKG"
			echo
		fi
	fi
done
echo

# interactive mode
if $MANUAL_MODE && [ -n "$BAD" ] ; then
	NR=0
	echo "Entering manual mode, filing $MAX bugs."
	for PKG in $BAD ; do
		SRC=$(grep-dctrl -sSource -FPackage -n $PKG --exact-match ${PACKAGES[2]} | cut -d " " -f1)
		if [ -z "$SRC" ] ; then
			SRC=$PKG
		fi
		if [ -x /usr/bin/firefox ] ; then
			firefox https://packages.debian.org/$PKG & :
	
			sleep 0.5
			firefox "https://bugs.debian.org/cgi-bin/pkgreport.cgi?dist=unstable;include=subject%3Atransitional;src=$SRC" & :
			sleep 1
		fi
		echo "firefox https://packages.debian.org/$PKG ;"
		let NR=$NR+1
		if [ $NR -eq $MAX ] ; then
			echo "Taking a break after $MAX packages."
			break
		fi
	done
	if [ ! -x /usr/bin/firefox ] ; then
		echo "Please open those firefox tabs… and press enter to continue."
		read a
	fi
	NR=0
	if [ -x /usr/bin/mutt ] ; then
		for PKG in $BAD ; do
			SRC=$(grep-dctrl -sSource -FPackage -n $PKG --exact-match ${PACKAGES[2]} | cut -d " " -f1)
			if [ -z "$SRC" ] ; then
				SRC=$PKG
			fi
			VERSION=$(grep-dctrl -sVersion -FPackage -n $PKG --exact-match ${PACKAGES[2]})
			VERBOSE=$( ( for SAUCE in ${PACKAGES[0]} ${PACKAGES[1]} ${PACKAGES[2]} ; do
				grep-dctrl -sPackage,Description,Version -FPackage $PKG --exact-match $SAUCE
				done ) | sort -u)
			# wording when filing bugs since the beginning of the development cycle
			WORDING="Please drop the transitional package $PKG (from the source package $SRC) for $NEXT, as it has been released with $OLDSTABLE and $STABLE already."
			# wording when filing bugs late in the development cycle
			#WORDING="Please drop the transitional package $PKG (from the source package $SRC) after the release of $NEXT, it has been released with $OLDSTABLE and $STABLE already..."
			TMPFILE=`mktemp`
			cat >> $TMPFILE <<- EOF
Package: $PKG
Version: $VERSION
Severity: normal
user: qa.debian.org@packages.debian.org
usertags: transitional

$(echo "$WORDING" | fold -s)

$VERBOSE

Thanks for maintaining $SRC!

EOF
			mutt -E -s "please drop transitional package $PKG from src:$SRC" -i $TMPFILE submit@bugs.debian.org
			rm -f $TMPFILE

			let NR=$NR+1
			if [ $NR -eq $MAX ] ; then
				break
			fi
		done
		echo -n "Filed $NR bugs against there packages:"
		nr=0
		for i in $BAD ; do
			echo -n " $i"
			let nr=$nr+1
			if [ $nr -eq $NR ] ; then
				break
			fi
		done
	fi
	echo
elif ! $MANUAL_MODE ; then
	# non-interactive mode
	for PKG in $BAD ; do
		echo
		( for SAUCE in ${PACKAGES[0]} ${PACKAGES[1]} ${PACKAGES[2]} ; do
			grep-dctrl -sPackage,Description,Version -FPackage $PKG --exact-match $SAUCE
		done ) | sort -u
	done
	echo
	echo $BAD | dd-list --sources ${SOURCES[2]} -i
fi

echo
echo "Found $BAD_COUNTER bad packages (= transitional dummy package in $OLDSTABLE, $STABLE and sid)"
echo "and $GOOD_COUNTER removed transitional packages (= don't exist in sid anymore, so we"
echo "assume they are no problem in $NEXT as well)."
echo "Plus we know about $BUGGED_COUNTER bugs about obsolete transitional packages we have"
echo "already filed, of which $OPEN_COUNTER are still unfixed in sid."
echo
echo "In the future, this script should probably also complain about packages"
echo "depending on transitional packages."

echo
if [ "${BASEPATH:0:5}" = "/tmp/" ] ; then
	rm $BASEPATH -r
else
	du -sch $BASEPATH
	echo "please rm $BASEPATH manually."
fi

if [ $BAD_COUNTER -gt 0 ] ; then
	echo "Warning: there seems to be $BAD_COUNTER unfiled bugs in $NEXT."
fi

echo
echo "All user-tagged bugs about transitional packages:"
echo "https://udd.debian.org/cgi-bin/bts-usertags.cgi?user=qa.debian.org%40packages.debian.org&tag=transitional"
echo
