#!/bin/bash
# vim: set noexpandtab:

# Copyright 2021-2023 Holger Levsen <holger@layer-acht.org>
# Copyright 2021-2022 Roland Clobus <rclobus@rclobus.nl>
# released under the GPLv2

# Coding convention: enforced by 'shfmt'

DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh
set -e
set -o pipefail # see eg http://petereisentraut.blogspot.com/2010/11/pipefail.html

# always cleanup
cleanup() {
	local RESULT=$1
	output_echo "Cleanup ${RESULT}"

	# publish results
	publish_results $RESULT

	# Cleanup the workspace
	if [ -n "${BUILDDIR}" ]; then
		sudo rm -rf --one-file-system ${BUILDDIR}
	fi
	# Cleanup the results
	if [ -n "${RESULTSDIR}" ]; then
		rm -rf --one-file-system ${RESULTSDIR}
	fi
}

# Copy a single file to the web server or delete it there
#
# Argument 1 = action
# Argument 2 = description (one word)
# Argument 3 = absolute path of a file that will be published on the web server
# Argument 4 = filename for the published file
publish_file() {
	local ACTION=$1
	local DESCRIPTION=$2
	local ORIGINAL_FILE=$3
	local PUBLISHED_NAME=$(basename "$4")

	# this uses bin/reproducible_debian_live_sync_result.sh via ~/.ssh/authorized_keys
	ssh jenkins@jenkins.debian.net "${ACTION}" "${DESCRIPTION}" "${ORIGINAL_FILE}" "${PUBLISHED_NAME}"
}

prepare_publishing() {
	# Remotely remove previously published files once if they don't exist locally
	[ ! -f ${DESKTOP}-${SUITE}.iso ] || publish_file delete ISOfile ${DESKTOP}-${SUITE}.iso
	[ ! -f ${DESKTOP}-${SUITE}-summary.txt ] || publish_file delete Summary ${DESKTOP}-${SUITE}-summary.txt
	[ ! -f ${DESKTOP}-${SUITE}.html ] || publish_file delete DiffoscopeOutput ${DESKTOP}-${SUITE}.html
	[ ! -f ${DESKTOP}-${SUITE}-env.txt ] || publish_file delete Environment ${DESKTOP}-${SUITE}-env.txt
}

publish_results() {
	local RESULT=$1
	output_echo "Publishing results: ${RESULT}"

	# cleanup files remotely if they don't exist locally
	prepare_publishing

	if [ "${RESULT}" == "success" ]; then
		output_echo "Info: no differences found."

		# Upload the ISO file and its summary to the web server
		ISONAME=${DESKTOP}-${SUITE}.iso
		publish_file publish ISOfile ${RESULTSDIR}/b1/${PROJECTNAME}/${DESKTOP}/live-image-amd64.hybrid.iso ${ISONAME}
		publish_file publish Summary ${RESULTSDIR}/summary_build1.txt ${DESKTOP}-${SUITE}-summary.txt

		# Invoke openQA
		CHECKSUM=$(grep "Checksum:" ${RESULTSDIR}/summary_build1.txt | cut -f 2 -d " ")
		IMAGE_TIMESTAMP=$(grep "Snapshot timestamp:" ${RESULTSDIR}/summary_build1.txt | cut -f 3 -d " ")
		case ${DESKTOP} in
		"smallest-build")
			# No DESKTOP=xxx setting, LIVE_INSTALLER=no
			OPENQA_EXTRA_OPTIONS="LIVE_INSTALLER=no"
			;;
		"standard")
			# No DESKTOP=xxx setting
			OPENQA_EXTRA_OPTIONS=""
			;;
		*)
			OPENQA_EXTRA_OPTIONS="DESKTOP=${DESKTOP}"
			;;
		esac
		output_echo "Triggering openqa.debian.net for live-build ${IMAGE_TIMESTAMP}_${SUITE}_${DESKTOP} checksum=${CHECKSUM} now."
		schroot --directory ${RESULTSDIR} -c source:jenkins-reproducible-unstable-diffoscope -- openqa-cli api -X POST isos ISO=${DESKTOP}_${SUITE}_${IMAGE_TIMESTAMP}.iso DISTRI=debian VERSION=${SUITE}_${DESKTOP} FLAVOR=live-build ARCH=x86_64 BUILD=${IMAGE_TIMESTAMP}_${SUITE}_${DESKTOP} CHECKSUM=${CHECKSUM} TIMESTAMP=${IMAGE_TIMESTAMP} ISO_URL=https://tests.reproducible-builds.org/debian/live_build/artifacts/r00t-me/${ISONAME} --odn --apikey ${OPENQA_APIKEY} --apisecret ${OPENQA_APISECRET} ${OPENQA_EXTRA_OPTIONS} | tee out.json
		jq .ids out.json | awk '$1 + 0 > 0 { print "- enjoy https://openqa.debian.net/tests/" ($1 + 0) }'
		jq .scheduled_product_id out.json  | awk '$1 + 0 > 0 { print "- enjoy https://openqa.debian.net/admin/productlog?id=" ($1 + 0) }'
		rm out.json
	else
		if [ -f "${RESULTSDIR}/${PROJECTNAME}/${DESKTOP}/live-image-amd64.hybrid.iso.html" ]; then
			# Publish the output of diffoscope, there are differences
			output_echo "Publishing the output of diffoscope, there are differences."
			publish_file publish DiffoscopeOutput ${RESULTSDIR}/${PROJECTNAME}/${DESKTOP}/live-image-amd64.hybrid.iso.html ${DESKTOP}-${SUITE}.html
		else
			output_echo "Error: Something went wrong."
			printenv >environment.txt
			publish_file publish Environment ${PWD}/environment.txt ${DESKTOP}-${SUITE}-env.txt
		fi
	fi
}

#
# main: follow https://wiki.debian.org/ReproducibleInstalls/LiveImages
#
# Randomize start time
delay_start

# Argument 1 = image type
export DESKTOP="$1"

# Argument 2 = Debian version
export SUITE="$2"

# Argument 3 = Debian-installer origin
export INSTALLER_ORIGIN="$3"

# Three arguments are required
if [ -z "${DESKTOP}" -o -z "${SUITE}" -o -z "${INSTALLER_ORIGIN}" ]; then
	output_echo "Error: Bad command line arguments."
	exit 1
fi
output_echo "Info: Building Debian live-build for $DESKTOP / $SUITE"

# DESKTOP option: Select the server for the repository
# Allowed values:
#  auto (default): Detect the best available server
#  snapshot (preferred): Use a snapshot server
#  archive (fallback): Use deb.debian.org if a snapshot server is not available
#
# Note that 'archive' allows only for a time window of about 6 hours to test for reproducibility
export REPOSERVER=archive

if [ "${REPOSERVER}" == "auto" ]; then
	output_echo "Info: Detecting whether to use snapshot.notset.fr or deb.debian.org"
	set +e # We are interested in the return value
	wget --quiet --no-hsts http://snapshot.notset.fr/mr/timestamp/debian/latest --output-document latest
	RETURNVALUE=$?
	rm -f latest
	set -e
	if [ ${RETURNVALUE} -eq 0 ]; then
		export REPOSERVER=snapshot
	else
		# The snapshot server is not available
		output_echo "Info: The snapshot server is not available, using deb.debian.org instead."
		export REPOSERVER=archive
	fi
fi

# Cleanup if something goes wrong
trap cleanup INT TERM EXIT

if $DEBUG; then
	export WGET_OPTIONS=
else
	export WGET_OPTIONS=--quiet
fi

# Generate and use an isolated workspace
export PROJECTNAME="live-build"
mkdir -p /srv/workspace/live-build
export BUILDDIR=$(mktemp --tmpdir=/srv/workspace/live-build -d -t ${DESKTOP}-${SUITE}.XXXXXXXX)
cd ${BUILDDIR}
export RESULTSDIR=$(mktemp --tmpdir=/srv/reproducible-results -d -t ${PROJECTNAME}-${DESKTOP}-${SUITE}-XXXXXXXX) # accessible in schroots, used to compare results

# Fetch the rebuild script (and nothing else)
output_echo "Fetching the rebuild script."
git clone https://salsa.debian.org/live-team/live-build.git rebuild_script --no-checkout --depth 1
cd rebuild_script
git checkout HEAD test/rebuild.sh
cd ..

# First build
output_echo "Running the rebuild script for the 1st build."
set +e # We are interested in the return value
# The 3rd argument is provided to allow for local testing of this script.
# - If SNAPSHOT_TIMESTAMP is already set, use that value
# - If SNAPSHOT_TIMESTAMP is not set, use REPOSERVER
export SNAPSHOT_TIMESTAMP="${SNAPSHOT_TIMESTAMP:-${REPOSERVER}}"
rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}" "${INSTALLER_ORIGIN}"
RETURNVALUE=$?
grep --quiet --no-messages "Build result: 0" summary.txt
BUILD_OK_FOUND=$?
set -e

if [ ${RETURNVALUE} -eq 99 -a "${SNAPSHOT_TIMESTAMP}" == "archive" ]; then
	# The archive was updated while building. Retry
	output_echo "Info: during the first build the archive was updated, now trying again."
	rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}" "${INSTALLER_ORIGIN}"
elif [ ${RETURNVALUE} -ne 0 -o ${BUILD_OK_FOUND} -ne 0 ]; then
	# Something went wrong. Perhaps an alternative timestamp is proposed
	SNAPSHOT_TIMESTAMP=$(grep --no-messages "Alternative timestamp:" summary.txt | cut -f 3 -d " ")
	if [ -z ${SNAPSHOT_TIMESTAMP} ]; then
		output_echo "Error: the image could not be built, no alternative was proposed."
		exit 1
	fi
	output_echo "Warning: the build failed with ${RETURNVALUE}. The latest snapshot might not be complete (yet). Now trying again using the previous snapshot instead."
	rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}" "${INSTALLER_ORIGIN}"
else
	# Determine the value of SNAPSHOT_TIMESTAMP for the second build
	# If 'archive' is used for the first build, use it for the second build too and hope that the same timestamp will still be available
	if [ "${SNAPSHOT_TIMESTAMP}" != "archive" ]; then
		export SNAPSHOT_TIMESTAMP=$(grep --no-messages "Snapshot timestamp:" summary.txt | cut -f 3 -d " ")
	fi
fi

# Move the image away
mkdir -p ${RESULTSDIR}/b1/${PROJECTNAME}/${DESKTOP}
mv live-image-amd64.hybrid.iso ${RESULTSDIR}/b1/${PROJECTNAME}/${DESKTOP}
mv summary.txt ${RESULTSDIR}/summary_build1.txt

# Second build
output_echo "Running the rebuild script for the 2nd build."
set +e # We are interested in the return value
rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}" "${INSTALLER_ORIGIN}"
RETURNVALUE=$?
set -e

if [ "${SNAPSHOT_TIMESTAMP}" == "archive" ]; then
	if [ ${RETURNVALUE} -eq 99 ]; then
		# The archive was updated while building.
		# This is unfortunate, another two (!) builds are required
		# Discard both builds and retry
		output_echo "Info: during the second build the archive was updated, now trying again."
		rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}" "${INSTALLER_ORIGIN}"
		mv live-image-amd64.hybrid.iso ${RESULTSDIR}/b1/${PROJECTNAME}/${DESKTOP}
		mv summary.txt ${RESULTSDIR}/summary_build1.txt
		rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}" "${INSTALLER_ORIGIN}"
	elif [ $(stat ${RESULTSDIR}/b1/${PROJECTNAME}/${DESKTOP}/live-image-amd64.hybrid.iso live-image-amd64.hybrid.iso | grep Modify: | uniq | wc -l) -ne 1 ]; then
		# The timestamps are different. Discard the first build and retry
		output_echo "Info: between the builds the archive was updated, now trying again."
		mv live-image-amd64.hybrid.iso ${RESULTSDIR}/b1/${PROJECTNAME}/${DESKTOP}
		mv summary.txt ${RESULTSDIR}/summary_build1.txt
		rebuild_script/test/rebuild.sh "$1" "$2" "${SNAPSHOT_TIMESTAMP}" "${INSTALLER_ORIGIN}"
	fi
fi

# Move the image away
mkdir -p ${RESULTSDIR}/b2/${PROJECTNAME}/${DESKTOP}
mv live-image-amd64.hybrid.iso ${RESULTSDIR}/b2/${PROJECTNAME}/${DESKTOP}
mv summary.txt ${RESULTSDIR}/summary_build2.txt

# Clean up
output_echo "Running lb clean after the 2nd build."
sudo lb clean --purge

# We are done
cd ..

if [ $(cat ${RESULTSDIR}/summary_build1.txt ${RESULTSDIR}/summary_build2.txt | grep Checksum: | uniq | wc -l) -ne 1 ]; then
	# The checksums are different. Run diffoscope on the images
	output_echo "Calling diffoscope on the results."
	TIMEOUT="240m"
	DIFFOSCOPE="$(schroot --directory /tmp -c chroot:jenkins-reproducible-${DBDSUITE}-diffoscope diffoscope -- --version 2>&1)"
	TMPDIR=${RESULTSDIR}
	call_diffoscope ${PROJECTNAME} ${DESKTOP}/live-image-amd64.hybrid.iso
fi

cleanup success

# Turn off the trap
trap - INT TERM EXIT

# We reached the end, return with PASS
exit 0
