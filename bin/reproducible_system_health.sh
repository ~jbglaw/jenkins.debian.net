#!/bin/bash
# vim: set noexpandtab:

# Copyright 2021-2023 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

###
###
###  calculate a number between 0 and 255 representing the health status
###  of https://tests.reproducible-builds.org for usage with
###  https://github.com/jelly/reproduciblebuilds-display/
### 
###  also produce a fancy and functional web page, showing more details:
###  https://tests.reproducible-builds.org/trbo.status.html
###
###

DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"
# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh
# we fail hard
set -e

# define some variables
HEALTH_FILE=$BASE/trbo.status
STATUS=-1
INPUTS=0
SCORE=0
INVALID=0
SUSPICIOUS=0
FAILED_JOBS=$(mktemp --tmpdir=$TMPDIR trbo-status-XXXXXXX)
UNSTABLE_JOBS=$(mktemp --tmpdir=$TMPDIR trbo-status-XXXXXXX)
IGNORED_JOBS=$(mktemp --tmpdir=$TMPDIR trbo-status-XXXXXXX)
KNOWN_BAD_JOBS=$(mktemp --tmpdir=$TMPDIR trbo-status-XXXXXXX)
FAILED_SUSPECTS=$(mktemp --tmpdir=$TMPDIR trbo-status-XXXXXXX)
UNSTABLE_SUSPECTS=$(mktemp --tmpdir=$TMPDIR trbo-status-XXXXXXX)
LOG=$(mktemp --tmpdir=$TMPDIR trbo-status-XXXXXXX)

small_note() {
	NOTE="<small>($1)</small>"
}

prepare_log(){
	if [ -f $JOB_NAME/builds/$LAST/log ] ; then
		cp $JOB_NAME/builds/$LAST/log $LOG
	elif [ -f $JOB_NAME/builds/$LAST/log.gz ] ; then
		zcat $JOB_NAME/builds/$LAST/log.gz > $LOG
	else
		echo > $LOG
	fi
}

# gather data
echo "$(date -u) - starting up."
cd /var/lib/jenkins/jobs/
for JOB_NAME in $(ls -1d reproducible_* | sort ) ; do
	JOB_URL="https://jenkins.debian.net/job/$JOB_NAME"
	REMOTE=false
	SUSPECT=false
	# ignore jobs known bad jobs (from jenkins configuration)
	if $(grep -q '<disabled>true</disabled>' $JOB_NAME/config.xml) ; then
			echo "1|$JOB_NAME|$JOB_URL||" >> ${KNOWN_BAD_JOBS}
			echo "  ignored job: $JOB_NAME (known to be non-functional)"
			let INVALID+=1
			continue
	fi
	# if a job is running remotely we will need to see if the node is online
	case $JOB_NAME in
		reproducible_node_health_check_amd64_jenkins|reproducible_maintenance_amd64_jenkins|reproducible_setup_schroot_*_diffoscope_amd64_jenkins)
			# local jobs
			:
			;;
		reproducible_setup_schroot_*_diffoscope_amd64_*)
			NODE_ALIAS=$(echo $JOB_NAME | cut -d '_' -f7)
			NODE_ARCH=$(echo $JOB_NAME | cut -d '_' -f6)
			REMOTE=true
			;;
		reproducible_node_health_check_*|reproducible_setup_pbuilder_*)
			NODE_ALIAS=$(echo $JOB_NAME | cut -d '_' -f6)
			NODE_ARCH=$(echo $JOB_NAME | cut -d '_' -f5)
			REMOTE=true
			;;
		reproducible_maintenance_*)
			NODE_ALIAS=$(echo $JOB_NAME | cut -d '_' -f4)
			NODE_ARCH=$(echo $JOB_NAME | cut -d '_' -f3)
			REMOTE=true
			;;
		*)
			:
			;;
	esac
	if $REMOTE ; then
		# sometimes the dns names are different than the jenkins names...
		case $NODE_ARCH in
			armhf)	NODE="$NODE_ALIAS-armhf-rb.debian.net" ;;
			*)	NODE="${NODE_ALIAS}-${NODE_ARCH}.debian.net" ;;
		esac
		# ignore jobs on known bad nodes:
		# check the offline list from git (and the one updated by jenkins)
		# as this should only ignore nodes humans (and not jenkins) have
		# acknowledged they are down...
		# jobs one nodes marked down by jenkins are remarked but still counted
		if grep -q $NODE $JENKINS_OFFLINE_GIT_LIST >/dev/null 2>&1 ; then
			echo "1|$JOB_NAME|$JOB_URL||" >> ${IGNORED_JOBS}
			echo "  ignored job: $JOB_NAME (node is marked offline)"
			let INVALID+=1
			continue
		elif grep -q $NODE $JENKINS_OFFLINE_LIST >/dev/null 2>&1 ; then
			SUSPECT=true
			let SUSPICIOUS+=1
			# we still count this job for the overall status...
		fi
	fi
	#
	# node is not known offline (in git), let's go
	#
	let INPUTS+=1
	FILE=$JOB_NAME/builds/permalinks
	LAST=$(grep lastCompletedBuild $FILE|awk '{print $2}')
	STABLE=$(grep lastStableBuild $FILE|awk '{print $2}')
	UNSTABLE=$(grep lastUnstableBuild $FILE|awk '{print $2}')
	NOTE=""
	if [ "$LAST" = "$STABLE" ] ; then
		echo "  stable job: $JOB_NAME"
		let SCORE+=3 || SCORE=0
	elif [ "$LAST" = "$UNSTABLE" ] ; then
		echo "unstable job: $JOB_NAME"
		let SCORE+=1 || SCORE=0
		prepare_log
		if [ "$JOB_NAME" = "reproducible_create_meta_pkg_sets" ] ; then
			if $(grep -q "^cp " $LOG) ; then
				small_note "outdated pkg set(s), manual intervention required"
			elif $(grep -q "could not download tail's latest packages file" $LOG) ; then
				small_note "problem with tails pkg set"
			elif $(grep -q "could not download cloud-image package list" $LOG) ; then
				small_note "problem with cloud-image pkg set"
			elif $(grep -q "could not download grml's latest dpkg.selections file" $LOG) ; then
				small_note "problem with grml pkg set"
			elif $(grep -E -q "could not (download|determine) PureOS" $LOG) ; then
				small_note "problem with PureOS pkg set"
			elif $(grep -q "Warning: dose-deb-coinstall cannot calculate" $LOG) ; then
				PKG_SET_PROBLEM=$(grep "Warning: dose-deb-coinstall cannot calculate" $LOG | cut -d "'" -f2 | cut -d ' ' -f3-11 | cut -d '.' -f1 | head -1 || true)
				small_note "$PKG_SET_PROBLEM"
			elif $(grep -E -q "could not update cloud-image package list" $LOG) ; then
				small_note "problem with cloud-image pkg set"
			fi
		# only show the most severe problem, don't aggregate them
		elif $(grep -q "^Warning: rise of the jenkins job zombies has started again" $LOG) ; then
			small_note "job zombies detected on jenkins"
		elif $(grep -q "failed Squid Web Proxy Server" $LOG) ; then
			small_note "squid.service failed"
		elif $(grep -q "^Warning: there are packages to be upgraded on this system" $LOG) ; then
			small_note "host needs apt-get dist-upgrade"
		elif $(grep -q "Kernel needs upgrade" $LOG) ; then
			small_note "reboot needed for kernel upgrade"
		elif $(grep -q "^WARNING: Running kernel does not match on-disk kernel image:" $LOG) ; then
			small_note "reboot needed for kernel upgrade"
		elif $(grep -q "^Warning: running kernel needs attention" $LOG) ; then
			small_note "running kernel needs attention"
		elif $(grep -q -E "Warning: too many kernels, .* should be removed." $LOG) ; then
			small_note "too many kernels, remove linux-image-$(grep 'Warning: too many kernels' $LOG | cut -d ',' -f2 | xargs echo)"
		elif $(grep -q "Warning: more than 1 kernel(s) in /boot" $LOG) ; then
			small_note "more than one kernel installed"
		elif $(grep -q "Warning: more than 3 kernel(s) in /boot" $LOG) ; then
			small_note "more than three kernel(s) installed"
		elif $(grep -q "Warning: more than 5 kernel(s) in /boot" $LOG) ; then
			small_note "more than five kernels installed"
		elif $(grep -q "failed failed /etc/rc.local Compatibility" $LOG) ; then
			small_note "rc-local.service failed"
		elif $(grep -E -q "failed Session [0-9]+ of user jenkins" $LOG) ; then
			small_note "session failed for user jenkins"
		elif $(grep -q "etckeeper.service loaded failed" $LOG) ; then
			small_note "etckeeper.service problem, manual intervention required"
		elif $(grep -E -q "^Warning: processes found which should not be there and which could not be killed." $LOG) ; then
			small_note "unkillable unwanted processes"
		elif $(grep -q "failed failed pbuilder_build" $LOG) ; then
			small_note "pbuilder build scope failed"
		elif $(grep -q "failed failed Avahi mDNS/DNS-SD Stack" $LOG) ; then
			small_note "avahi failed"
		elif $(grep -q "failed failed Rotate log files" $LOG) ; then
			small_note "logrotate failed"
		elif $(grep -q "^Warning: failed to end schroot session:" $LOG) ; then
			small_note "failed to end schroot session"
		elif $(grep -q "Warning: Tried, but failed to delete these in /var/lib/schroot/" $LOG) ; then
			small_note "failed to delete old schroot session data"
		elif $(grep -q "Warning: Tried, but failed to delete these schroots:" $LOG) ; then
			small_note "failed to delete schroots"
		elif $(grep -q "Warning: Tried, but failed to delete these schroot sessions:" $LOG) ; then
			small_note "failed to delete schroot sessions"
		elif $(grep -q "Warning: Tried, but failed to delete these sbuild directories:" $LOG) ; then
			small_note "failed to delete sbuild dirs"
		elif $(grep -q "^UNKNOWN: Failed to get a version string from image /boot/vmlinuz" $LOG) ; then
			small_note "/boot/vmlinuz* not readable for check-running-kernel"
		elif $(grep -q "Warning: found reproducible_build.sh processes which have pid 1 as parent (and not sshd)" $LOG) ; then
			small_note "reproducible_build.sh zombies"
		elif $(grep -q "State: starting" $LOG) ; then
			small_note "node in starting state"
		elif $(grep -q "Warning: systemd is reporting errors" $LOG) ; then
			small_note "undefined service problems"
		elif $(grep -E -q "Warning: failed to update .* chdist." $LOG) ; then
			small_note "failed to update Debian chdist"
		elif $(grep -q "Warning: failed to update Arch Linux schroot, pacman/db.lck exists." $LOG) ; then
			small_note "pacman/db.lck exists, thus failing to update Arch Linux schroot, manual intervention required"
		elif $(grep -q "Warning: failed to update Arch Linux schroot." $LOG) ; then
			small_note "failed to update Arch Linux schroot"
		elif $(grep -q "Warning: today is the wrong future." $LOG) ; then
			small_note "host date is the wrong future date"
		elif $(grep -q "Warning: today is the wrong present" $LOG) ; then
			small_note "please update real_year in reproducible_node_health_check.sh"
		elif $(grep -E -q "Warning: today .* came back to the present:" $LOG) ; then
			small_note "Warning: host date is correct, while it should be in future"
		elif $(grep -q "Warning: sbuild failed. Exiting cleanly as this is out-of-scope" $LOG) ; then
			UNSAT_DEPENDS=$(grep "unsat-dependency:" $LOG || true)
			if [ -n "$UNSAT_DEPENDS" ] ; then
				small_note "sbuild failed due to $UNSAT_DEPENDS"
			elif $(grep -E -q "^Err http://snapshot.debian.org.*Undetermined Error" $LOG) ; then
				small_note "sbuild failed due to snapshot.d.o throttling access"
			else
				small_note "sbuild failed for undetected reasons"
			fi
		elif $(grep -q "^Warning: no .buildinfo exists" $LOG) ; then
			small_note ".buildinfo not available"
		elif $(grep -E -q "^(W|Err): .*503  Service Unavailable.* 3128" $LOG) ; then
			small_note "issues affecting the http(s) proxy used"
		elif $(grep -E -q "^(W|E): diffoscope" $LOG) ; then
			small_note "lintian issues in src:diffoscope"
		elif $(grep -q "Warning: diffoscope detected differences in the images" $LOG) ; then
			small_note "diffoscope detected differences in the images"
		elif $(grep -E -q "UserWarning: .* is running against .* this may cause problems" $LOG) ; then
			small_note "python library warning during build"
		elif $(grep -E -q "DeprecationWarning: The distutils package is deprecated and slated for removal" $LOG) ; then
			small_note "python deprecation warning during build"
		elif $(grep -E -q "SetuptoolsDeprecationWarning.*is deprecated, please" $LOG) ; then
			small_note "python deprecation warning during build"
		elif $(grep -E -q "^[a-z]+.cpp:[0-9]+:[0-9]+: warning:" $LOG) ; then
			small_note "compiler warning during build"
		elif $(grep -q "^Warning: The following builds have failed due to diffoscope schroot problems and will be rescheduled now:" $LOG) ; then
			small_note "found and rescheduled build failures due to diffoscope schroot problems"
		elif $(grep -q "Warning: Found files with bad permissions" $LOG) ; then
			small_note "files with bad permissions found"
		fi
		if ! $SUSPECT ; then
			echo "1|$JOB_NAME|$JOB_URL|true|$NOTE" >> ${UNSTABLE_JOBS}
		else
			echo "1|$JOB_NAME|$JOB_URL||$NOTE" >> ${UNSTABLE_SUSPECTS}
		fi
	else
		prepare_log
		# only show the most severe problem, don't aggregate them
		if $(grep -E -q "Failed to connect to [.0-9]+ port 3128: Connection refused" $LOG) ; then
			small_note "failed to connect to https-proxy"
		elif $(grep -q "seems to be down, sleeping" $LOG) ; then
			small_note "node seemed down"
		elif $(grep -q "Build timed out (after 15 minutes). Marking the build as aborted." $LOG) ; then
			small_note "job timed out"
		elif $(tail -1 $LOG | grep -q "Finished: ABORTED") ; then
			small_note "job was aborted"
		elif $(grep -q "Caused: hudson.remoting.ChannelClosedException" $LOG) ; then
			small_note "jenkins shut down while job was running"
		elif $(grep -q "java.io.IOException: No space left on device" $LOG) ; then
			small_note "diskspace issue on main jenkins host"
		elif $(grep -q "^make -r world: build failed. Please re-run" $LOG) ; then
			small_note "make world failed"
		elif $(grep -q "^ERROR: Failed to make build_install" $LOG) ; then
			small_note "make build_install failed"
		elif $(grep -q "^ERROR: Failed to make release" $LOG) ; then
			small_note "make release failed"
		elif $(grep -E -q "ERROR: tools/.* failed to build" $LOG) ; then
			small_note "make tools failed"
		elif $(grep -E -q "ERROR: target/.* failed to build" $LOG) ; then
			small_note "some target failed"
		elif $(grep -E -q "make\[[0-9]+\]: .*package/install\] Error 2" $LOG) ; then
			small_note "make package/install failed"
		elif $(grep -q "^Errors occurred, no packages were upgraded." $LOG) ; then
			small_note "no packages were upgraded"
		elif $(grep -q "CRITICAL: Unknown exception found" $LOG) ; then
			small_note "unknown exception"
		elif $(grep -q "^dpkg-buildpackage: error:" $LOG) ; then
			small_note "dpkg-buildpackage failed"
		elif $(grep -q "Failed to connect to review.coreboot.org port 443" $LOG) ; then
			small_note "failed to connect to review.coreboot.org"
		elif $(grep -E -q "Error: lb build failed with .*, even with an older snapshot." $LOG) ; then
			small_note "live builds failed, both using current and older snapshots."
		elif $(grep -E -q "^building build_cdrom_.* failed, see log file" $LOG) ; then
			small_note "d-i cdrom build failed."
		elif $(grep -q "^E: Sub-process /usr/bin/dpkg returned an error code" $LOG) ; then
			small_note "dpkg failed"
		elif $(grep -q "^SSH EXIT CODE:" $LOG) ; then
			if $(grep -q "^Timeout, server .*.debian.net not responding." $LOG) || \
			 $(grep -q "^packet_write_wait: Connection to .*: Broken pipe" $LOG) ; then
				small_note "node stopped responding"
			elif $(grep -q "^unsupported host, exiting." $LOG) ; then
				small_note "unsupported host (hostname changed?)"
			elif $(grep -q "the end, diffoscope has returned 2" $LOG) ; then
				small_note "diffoscope failed with exit code 2"
			elif $(grep -q "^E: Unmet dependencies." $LOG) ; then
				small_note "apt failed: broken dependencies"
			elif $(grep -E -q "^E: The repository.*is not signed." $LOG) ; then
				small_note "problem with apt repository signature"
			elif $(grep -q "^error: RPC failed; HTTP 504 curl 22 The requested URL returned error: 504" $LOG) ; then
				small_note "git clone failed"
			elif $(grep -q "Error: Something went wrong." $LOG) ; then
				small_note "unspecific live-build failure"
			else
				small_note "SSH failure"
			fi
		elif $(grep -q "^psql: error: could not connect to server: No route to host" $LOG) ; then
			if $(grep -q 'Is the server running on host "udd-mirror.debian.net"' $LOG) ; then
				small_note "psql: could not connect to UDD-mirror.debian.net"
			else
				small_note "psql: could not connect to server"
			fi
		elif $(grep -q "^kex_exchange_identification: Connection closed by remote host" $LOG) ; then
				small_note "ssh login to remote host failed"
		elif $(grep -E -q "^ssh: connect to host .* No route to host" $LOG) ; then
				small_note "no route to host"
		elif $(grep -E -q "^ssh: Could not resolve hostname .*: Temporary failure in name resolution" $LOG) ; then
				small_note "failure in name resolution"
		elif $(grep -E -q "^ssh: connect to host .* Connection refused" $LOG) ; then
				small_note "ssh connection refused"
		elif $(grep -q "^E: Package 'diffoscope' has no installation candidate" $LOG) ; then
			small_note "package 'diffoscope' has no installation candidate"
		elif $(grep -q "^W: No exported results found in /tmp/job-exports" $LOG) ; then
			small_note "build failed"
		elif $(grep -q "^client_loop: send disconnect: Broken pipe" $LOG) ; then
			small_note "broken pipe"
		fi
		case $JOB_NAME in
			reproducible_maintenance_amd64_jenkins)			MODIFIER=250 ;; # main node
			reproducible_maintenance_amd64_ionos1)			MODIFIER=500 ;;	# proxy for other nodes
			reproducible_maintenance_amd64_ionos10)			MODIFIER=500 ;;	# proxy for other nodes
			reproducible_maintenance_amd64_*)			MODIFIER=20 ;;
			reproducible_maintenance_i386_*)			MODIFIER=10 ;;
			reproducible_maintenance_arm64_codethink16)		MODIFIER=500 ;; # proxy for other nodes
			reproducible_maintenance_arm64_*)			MODIFIER=10 ;;
			reproducible_maintenance_armhf_*)			MODIFIER=3 ;;
			reproducible_node_health_check_amd64_jenkins)		MODIFIER=150 ;;
			reproducible_node_health_check_amd64_ionos1)		MODIFIER=150 ;;
			reproducible_node_health_check_amd64_ionos10)		MODIFIER=150 ;;
			reproducible_node_health_check_amd64_*)			MODIFIER=20 ;;
			reproducible_node_health_check_i386_*)			MODIFIER=10 ;;
			reproducible_node_health_check_arm64_codethink16)	MODIFIER=150 ;;
			reproducible_node_health_check_arm64_*)			MODIFIER=10 ;;
			reproducible_node_health_check_armhf_*)			MODIFIER=3 ;;
			*)							MODIFIER=1  ;;
		esac
		if [ $MODIFIER -eq 0 ] ; then
			# skip silently, ignore those jobs
			let INPUTS-=1
			continue
		else
			if [ $MODIFIER -eq 1 ] ; then
				echo "  failed job: $JOB_NAME"
			else
				echo "  failed job: $JOB_NAME - $MODIFIER"
			fi
			if ! $SUSPECT ; then
				echo "$MODIFIER|$JOB_NAME|$JOB_URL|true|$NOTE" >> ${FAILED_JOBS}
			else
				echo "$MODIFIER|$JOB_NAME|$JOB_URL||$NOTE" >> ${FAILED_SUSPECTS}
			fi
		fi
		let SCORE-=$MODIFIER || SCORE=0
	fi
	if $DEBUG ; then
		echo SCORE=$SCORE
	fi
done
echo "$(date -u) - data gathered, raw score is $SCORE."
# represent data
if [ $SCORE -lt 0 ] ; then SCORE=0 ; fi
STATUS=$(echo "scale=3 ; $SCORE / ( $INPUTS * 3 ) * 255" | bc | cut -d '.' -f1)
GREEN=$STATUS
RED=$(echo 255-$STATUS|bc)
echo "$(date -u) - INPUTS = $INPUTS"
echo "$(date -u) - INVALID = $INVALID"
echo "$(date -u) - SCORE  = $SCORE"
echo "$(date -u) - STATUS = $STATUS"
echo $STATUS > $HEALTH_FILE
echo "$(date -u) - $HEALTH_FILE updated."

SUSPICIOUS_TEXT=""
if [ $SUSPICIOUS -gt 0 ] ; then
	SUSPICIOUS_TEXT="(including $SUSPICIOUS on nodes automatically marked offline)"
fi

cat > $HEALTH_FILE.html <<- EOF
<html>
<head>
 <meta charset="utf-8">
 <meta http-equiv="refresh" content="300">
</head>
<body style="background-color: rgb($RED, $GREEN, 0);">
 <h1>tests.reproducible-builds.org status summary view</h1>
 <h2>
  Status: <a href="$REPRODUCIBLE_URL/$(basename $HEALTH_FILE)">$STATUS</a> <small>(an integer between 0 and 255)</small>
 </h2>
 <p><small>
  Score: $SCORE
  <br/>
  Jobs considered: $INPUTS
  ${SUSPICIOUS_TEXT}
  <br/>
  Jobs ignored: $INVALID
 </small></p>
 <hr>
EOF

write2healthfile() {
	echo "$1" >> $HEALTH_FILE.html
}

sed_rb(){
	# layers and layers and layers...
	# but 2021 it's indeed easier to change the presentation of the names,
	# than the names...
	echo -n "$1" | sed \
		-e 's#reproducible#r-b#' \
		-e 's#openwrt-target#openwrt#' \
		-e 's#node_health_check#node_health#' \

}

conditional_paragraph() {
	if [ -s $1 ] ; then
		write2healthfile " <p>"
		if [ "${2:0:12}" = "Ignored jobs" ] ; then
			write2healthfile " <small>"
		fi
		write2healthfile "  $2:"
		write2healthfile "  <ul>"
		local AHREF=""
		local GROUP_COUNTER=0
		OFS=$IFS
		IFS=$'\012'
		for LINE in $(cat $1 | sort -t '|' -n ) ; do
			# only the first three fields are mandatory:
			# MODIFIER | JOB_NAME | JOB_URL | BADGE (true or false) | NOTE
			local MODIFIER=$(echo $LINE | cut -d '|' -f1)
			local NAME=$(echo $LINE | cut -d '|' -f2)
			local URL=$(echo $LINE | cut -d '|' -f3)
			if [ "${2:0:12}" = "Ignored jobs" ] ; then
				# we group ignored jobs so that they take up less visual space in the output
				local JOB_GROUP=$(echo $NAME |rev | cut -d '_' -f2-|rev)
				GROUP_MEMBERS=$(grep -c "${JOB_GROUP}_" $1)
				if [ $GROUP_MEMBERS -gt 1 ] ; then
					let GROUP_COUNTER+=1
					local JOB_SUFFIX=$(echo $NAME |rev | cut -d '_' -f1|rev)
					if [ -z "$AHREF" ] ; then
						AHREF=" $(sed_rb ${JOB_GROUP})_ <a href=\"$URL/\">${JOB_SUFFIX}</a>"
					else
						AHREF="$AHREF <a href=\"$URL/\">${JOB_SUFFIX}</a>"
					fi
					if [ $GROUP_COUNTER -ne $GROUP_MEMBERS ] ; then
						# not enough group members found
						continue
					fi
					# else, we are done grouping
				else
					# not part of a group
					AHREF="<a href=\"$URL/\">$(sed_rb $NAME)</a>"
				fi
			else
				local BADGE=$(echo $LINE | cut -d '|' -f4)
				local NOTE=$(echo $LINE | cut -d '|' -f5)
				if [ "$BADGE" = "true" ] ; then
					local LINK="<img src=\"$URL/badge/icon\">$(sed_rb $NAME)"
				else
					local LINK="$(sed_rb $NAME)"
				fi
				if [ "$MODIFIER" = "1" ] ; then
					local MOREINFO="$NOTE"
				else
					local MOREINFO="$NOTE <em>($MODIFIER)</em>"
				fi
				AHREF="<a href=\"$URL/\">$LINK</a> $MOREINFO"
			fi
			write2healthfile "   <li>$AHREF</li>"
			# reset variables for next job (group)
			AHREF=""
			GROUP_COUNTER=0
		done
		IFS=$OFS
		write2healthfile "  </ul>"
		if [ "${2:0:12}" = "Ignored jobs" ] ; then
			write2healthfile " </small>"
		fi
		write2healthfile " </p>"
	fi
}

conditional_paragraph ${FAILED_JOBS} "Failed jobs"
conditional_paragraph ${UNSTABLE_JOBS} "Unstable jobs"
conditional_paragraph ${FAILED_SUSPECTS} "Failed jobs on nodes automatically marked down by jenkins (with modifier > 1)"
conditional_paragraph ${UNSTABLE_SUSPECTS} "Unstable jobs on nodes automatically marked down by jenkins (with modifier > 1)"
write2healthfile "<hr><p>A stable jobs adds 3 to the score, an unstable job adds 1 and a failed job substracts something between 1 and 500 (indicated in brackets after the job name if not equal 1), depending on the importance of the job for the setup. 
</br>If the final score is below zero it will be set to zero.
</br>Finally, status is calculated by diving the score by three times the number of considered jobs and this gets multiplied with 255 to get a status between 0 and 255.
</br><small>(Ignored jobs are not counted at all.)</small>
</p><hr>"
conditional_paragraph ${IGNORED_JOBS} "Ignored jobs, because the nodes these are running on are <a href=\"https://salsa.debian.org/qa/jenkins.debian.net/-/blob/master/jenkins-home/offline_nodes\">documented</a> to be offline"
conditional_paragraph ${KNOWN_BAD_JOBS} "Ignored jobs, because they disabled for reasons"
write2healthfile "<hr><p><small>This page was last updated on $(date -u) by the <a href=\"https://jenkins.debian.net/job/reproducible_system_health/\">reproducible_system_health</a> job.</small></p>"
write2healthfile "</body></html>"
echo "$(date -u) - $(basename $HEALTH_FILE).html updated, visible at $REPRODUCIBLE_URL/$(basename $HEALTH_FILE).html."
echo "$(date -u) - the end."
rm -f ${FAILED_JOBS} ${UNSTABLE_JOBS} ${IGNORED_JOBS} ${KNOWN_BAD_JOBS} ${FAILED_SUSPECTS} ${UNSTABLE_SUSPECTS} $LOG
