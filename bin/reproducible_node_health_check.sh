#!/bin/bash
# vim: set noexpandtab:

# Copyright 2014-2023 Holger Levsen <holger@layer-acht.org>
#         © 2015-2018 Mattia Rizzolo <mattia@mapreri.org>
# released under the GPLv2

DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh

# to have a list of the nodes running in the future
. /srv/jenkins/bin/jenkins_node_definitions.sh

# some defaults
DIRTY=false
REP_RESULTS=/srv/reproducible-results

show_fstab_and_mounts() {
	echo "################################"
	echo "/dev/shm and /run/shm on $HOSTNAME"
	echo "################################"
	ls -lartd /run/shm /dev/shm/
	echo "################################"
	echo "/etc/fstab on $HOSTNAME"
	echo "################################"
	cat /etc/fstab
	echo "################################"
	echo "mount output on $HOSTNAME"
	echo "################################"
	mount
	echo "################################"
	DIRTY=true
}

#
# fail hard
#
set -e

#
# is the filesystem writetable?
#
echo "$(date -u) - testing whether /tmp is writable..."
TEST=$(mktemp --tmpdir=/tmp rwtest-XXXXXX)
if [ -z "$TEST" ] ; then
	echo "Failure to write a file in /tmp, assuming read-only filesystem."
	exit 1
fi
rm $TEST > /dev/null

#
# check for /dev/shm being mounted properly
#
echo "$(date -u) - testing whether /dev/shm is mounted correctly..."
mount | grep -E -q "^tmpfs on /dev/shm"
if [ $? -ne 0 ] ; then
	echo "Warning: /dev/shm is not mounted correctly on $HOSTNAME, it should be a tmpfs, please tell the jenkins admins to fix this."
	show_fstab_and_mounts
fi
test "$(stat -c %a -L /dev/shm)" = 1777
if [ $? -ne 0 ] ; then
	echo "Warning: /dev/shm is not mounted correctly on $HOSTNAME, it should be mounted with 1777 permissions, please tell the jenkins admins to fix this."
	show_fstab_and_mounts
fi
#
# check for /run/shm being a link to /dev/shm
#
echo "$(date -u) - testing whether /run/shm is a link..."
if ! test -L /run/shm ; then
	echo "Warning: /run/shm is not a link on $HOSTNAME, please tell the jenkins admins to fix this."
	show_fstab_and_mounts
elif [ "$(readlink /run/shm)" != "/dev/shm" ] ; then
	echo "Warning: /run/shm is a link, but not pointing to /dev/shm on $HOSTNAME, please tell the jenkins admins to fix this."
	show_fstab_and_mounts
fi

#
# check for hanging mounts
#
echo "$(date -u) - testing whether running 'mount' takes forever..."
timeout -s 9 15 mount > /dev/null
TIMEOUT=$?
if [ $TIMEOUT -ne 0 ] ; then
	echo "$(date -u) - running 'mount' takes forever, giving up."
	exit 1
fi

#
# check for correct future
#
# (XXX: yes this is hardcoded but meh…)
echo "$(date -u) - testing whether the time is right..."
get_node_information "$HOSTNAME"
real_year=2023
year=$(date +%Y)
if "$NODE_RUN_IN_THE_FUTURE"; then
	if [ "$year" -eq "$real_year" ]; then
		echo "Warning: today $HOSTNAME came back to the present: $(date -u)."
		DIRTY=true
	elif [ "$year" -eq "$((real_year + 1))" ] || \
		 [ "$year" -eq "$((real_year + 2))" -a "$(date +%m)" -le 2 ]; then
		echo "Good, today is the right future: $(date -u)."
	else
		echo "Warning: today is the wrong future: $(date -u)."
		DIRTY=true
	fi
else
	if [ "$year" -eq "$real_year" ]; then
		echo "Host is running in the present as it should: $(date -u)."
	else
		echo "Warning: today is the wrong present: $(date -u)."
		DIRTY=true
	fi
fi

#
# check for cleaned up kernels
#
echo "$(date -u) - testing whether too many kernels are installed..."
KERNEL="$(ls /boot/vmlinuz-*|wc -l)"
case $HOSTNAME in
	jenkins|ionos*)		MAX_KERNEL=5	;;
	codethink*)		MAX_KERNEL=1	;;
	osuosl*)		MAX_KERNEL=4	;;
	*)			MAX_KERNEL=3	;;
esac
if [ $KERNEL -gt $MAX_KERNEL ] ; then
	RUNNING=$(uname -r)
	OLDEST_KERNEL=$(ls /boot/vmlinuz-*|sed 's#/boot/vmlinuz-##g'|head -1)
	if [ "$RUNNING" != "$OLDEST_KERNEL" ] ; then
		echo "Warning: too many kernels, $OLDEST_KERNEL should be removed."
	else
		echo "Warning: more than $MAX_KERNEL kernel(s) in /boot"
	fi
	ls -lart /boot/vmlinuz-*
	df -h /boot
	echo "Running kernel: $(uname -r)"
	DIRTY=true
fi

#
# check if the latest kernel is running
#
if ! $(dpkg -l |grep linux-image | grep -q ^hi) ; then
	echo "$(date -u) - testing whether the latest kernel is running..."
	if ! /usr/lib/nagios/plugins/check_running_kernel ; then
		echo "Warning: running kernel needs attention!"  # string checked with logparse
		DIRTY=true
	fi
else
	echo "$(date -u) - a linux-image package has been manually put on hold, so no testing whether the latest kernel is running..."
fi

#
# check whether all services are running fine
#
echo "$(date -u) - checking whether all services are running fine..."
if ! systemctl is-system-running > /dev/null; then
	SERVICES=$(mktemp --tmpdir=$TEMPDIR node-health-XXXXXXX)
	systemctl list-units --state=error,failed | grep " failed " > $SERVICES || true
	echo "$(date -u) - problematic services found:"
	cat $SERVICES
	echo "$(date -u) - trying to fix problematic services."
	for UNIT in avahi-daemon acpid rtkit-daemon networking systemd-journal-flush haveged e2scrub_all apt-daily apt-daily-upgrade logrotate man-db; do
		if grep -q $UNIT $SERVICES ; then
			echo "$(date -u) - restarting failed service $UNIT..."
		        sudo systemctl restart $UNIT
		fi
	done
	for UNIT in rc-local ; do
		if grep -q $UNIT $SERVICES ; then
			echo "$(date -u) - resetting failed unit $UNIT..."
		        sudo systemctl reset-failed $UNIT
		fi
	done
	for UNIT in pbuilder_build pbuilder_update pbuilder_create session- user-runtime-dir@ user@ ; do
		if grep -q $UNIT $SERVICES ; then
			SCOPE=$(grep $UNIT $SERVICES | cut -d ' ' -f2)
			echo "$(date -u) - resetting failed scope $SCOPE for $UNIT..."
		        sudo systemctl reset-failed $SCOPE
		fi
	done
	if ! systemctl is-system-running > /dev/null; then
		systemctl status|head -5
		echo "Warning: systemd is reporting errors:"
		systemctl list-units --state=error,failed > $SERVICES
		if ! grep -q '0 loaded units listed' $SERVICES ; then
			cat $SERVICES
		fi
		systemctl list-jobs > $SERVICES
		if ! grep -q '0 loaded units listed' $SERVICES ; then
			cat $SERVICES
		fi
		echo "Manual cleanup needed."
		DIRTY=true
	fi
	rm $SERVICES
fi

# checks only for the main node
#
if [ "$HOSTNAME" = "$MAINNODE" ] ; then
	echo "$(date -u) - checking for known jenkins problems..."
	jenkins_bugs_check
fi

case "$HOSTNAME" in
	virt32a*)
		# TODO generalize to all nodes that need it
		echo "$(date -u) - checking for broken ssh port..."
		if ! grep -q 'Port 2273' /etc/ssh/sshd_config ; then
			echo "Warning: something has broken the ssh port configuration on $HOSTNAME"
			DIRTY=true
		fi
		;;
	*)	;;
esac

#
# finally
#
if ! $DIRTY ; then
	echo "$(date -u ) - Everything seems to be fine."
	echo
fi

echo "$(date -u) - the end."
