#!/bin/bash

# Copyright 2022 Mattia Rizzolo <mattia@debian.org>
# Copyright 2022-2023 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh

set -u
set -e
set -o pipefail # see eg http://petereisentraut.blogspot.com/2010/11/pipefail.html

PROJECT_NAME=debian_live_build
PROJECT_PATH=debian/live_build
NODE=osuosl3-amd64.debian.net
RBUILDLOG=/dev/null

# Argument 1 = description
# Argument 2 = absolute path on $NODE
# Argument 3 = published name
rsync_remote_results() {
    local description=$1
    local origfile=$2
    local filename=$(basename "$3")
    echo "$(date -u) - Starting to sync $description to '$filename'."
    cd "$BASE"/"$PROJECT_PATH"
    if [ "$description" == "ISOfile" ] ; then
        local EXTRADIR=artifacts/r00t-me/
        mkdir -p $EXTRADIR
        cd $EXTRADIR
        # Generate a warning
        local msg="These generated ISO files have been preserved for your convenience.\n"
        msg="${msg}They will be available for a few hours only, so download them now.\n"
        msg="${msg}WARNING: You shouldn't trust the ISO files downloaded from this host, they could contain malware or anything else, including the worst of your fears, packaged nicely as a bootable ISO image."
        log_info "$msg"
        # Place the warning as a heading in the directory view
        # This is embedded HTML, so no <HTML>-tag etc. are required
        PAGE=".HEADER.html"
        rm -f ${PAGE}
        write_page "<p>"
        write_page "$(printf "${msg}" | sed 's#$#<br/>#g')"
        write_page "These artifacts were created by one of the <a href=\"https://jenkins.debian.net/view/live/\">live-build jobs</a>"
        write_page "</p>"
    else
        local EXTRADIR=""
    fi
    local URL="${REPRODUCIBLE_URL}/${PROJECT_PATH}/$EXTRADIR${filename}"
    # Copy the new results from the build node to the web server node
    scp -p -o Batchmode=yes "$NODE":"$origfile" "$filename.tmp"
    chmod 755 "$filename.tmp"
    mv "$filename.tmp" "$filename"
    echo "$(date -u) - enjoy $URL"
}

# Argument 1 = description
# Argument 2 = filename part of the published name that was synced with 'rsync_remote_results'
delete_live_build_file() {
    local description=$1
    local filetodelete=$2
    local filename=$(basename "$filetodelete")
    echo "$(date -u) - Delete $description: '$filename'."
    cd "$BASE"/"$PROJECT_PATH"
    if [[ "$filename" != "$filetodelete" ]]; then
        echo "E: You provided a full path, ignoring for safety" >&2
        exit 1
    fi
    if [ "$description" == "ISOfile" ] ; then
        local EXTRADIR=artifacts/r00t-me/
        mkdir -p $EXTRADIR
        cd $EXTRADIR
    fi
    if [ -f "$filename" ]; then
        rm -vf "$filename"
	# also delete .tmp file if it exists
	[ ! -f "$filename.tmp" ] || rm -vf "$filename.tmp"
    else
        echo "E: File not found" >&2
        exit 1
    fi
}

parse_arguments() {
    if [ "$1" == "delete" ]; then
        shift
        delete_live_build_file "$@"
    elif [ "$1" == "publish" ]; then
        shift
        rsync_remote_results "$@"
    else
        echo "Undefined action: $1"
        exit 1
    fi
}

# main

# Intentionally unquoted, so the space-separated arguments will be available individually
parse_arguments ${SSH_ORIGINAL_COMMAND}

