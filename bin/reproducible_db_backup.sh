#!/bin/bash
# vim: set noexpandtab:

# Copyright 2014-2021 Holger Levsen <holger@layer-acht.org>
#         © 2015-2021 Mattia Rizzolo <mattia@debian.org>
# released under the GPLv2

DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh

#
# we fail hard
#
set -euo pipefail

_log() {
	echo "$(date -u) - $*"
}

# backup db
if [ "$HOSTNAME" = "$MAINNODE" ] ; then
	_log "backup db and update public copy."
	echo

	# prepare backup
	BACKUPDIR=$BASE/debian/backups
	mkdir -p $BACKUPDIR

	# keep 30 days and the 1st of the month
	_log "Deleting old backups…"
	DAY=$(date -d "30 day ago" '+%d')
	DATE=$(date -d "30 day ago" '+%Y-%m-%d')
	BACKUPFILE="$BACKUPDIR/reproducible_$DATE.sql.xz"
	if [ "$DAY" != "01" ] &&  [ -f "$BACKUPFILE" ] ; then
		rm -fv "$BACKUPFILE"
	fi

	# Make a daily backup of database
	DATE=$(date '+%Y-%m-%d')
	BACKUPFILE="$BACKUPDIR/reproducible_$DATE.sql"
	if [ -f "$BACKUPFILE.xz" ] ; then
		_log "The backup file $BACKUPFILE.xz already exists, skipping."
	else
		_log "Starting the backup now…"
		set -x
		pg_dump -x -O "$PGDATABASE" > "$BACKUPFILE"
		xz "$BACKUPFILE"

		# make the backup public
		ln -s -f -v "$BACKUPFILE.xz" $BASE/reproducible.sql.xz

		# recreate documentation of database
		# disabled since postgresql_autodoc is not available in bullseye
		# https://bugs.debian.org/970870
		#postgresql_autodoc -d "$PGDATABASE" -t html -f "$BASE/reproducibledb"
		set +x
	fi
	echo
	_log "$(date -u) - done."
else
	_log "This script shouldn't be run outside of $MAINNODE"
	exit 1
fi
